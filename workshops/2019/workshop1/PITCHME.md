# Code Club

## 2019 #1

[View presentation](https://gitpitch.com/grottrup/codeclub?grs=gitlab)


---
# Markup languages
---
## LaTeX Math

`\[
\left( \sum_{k=1}^n a_k b_k \right)^{\!\!2} \leq
 \left( \sum_{k=1}^n a_k^2 \right) \left( \sum_{k=1}^n b_k^2 \right)
\]`

$$1+1$$

```math
a^2+b^2=c^2
```

$`a^2+b^2=c^2`$
---
# Editors

---